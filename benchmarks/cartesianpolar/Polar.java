package benchmarks.cartesianpolar;

public class Polar {

  double radius;
  double theta; //angle in degree

  public Polar(double radius, double theta) {
    this.radius = radius;
    this.theta = theta;
  }

  /*@ public normal_behaviour
    @ requires !\fp_nan(this.radius) && !\fp_infinite(this.radius) &&
    @ this.theta < 270.0 && this.theta > 0.0;
    @ ensures !\fp_nan(\result.x) && !\fp_nan(\result.y);
    @*/
  public Cartesian toCartesian() {

    double radiant = this.theta * (Math.PI / 180.0);
    double x = this.radius * Math.cos(radiant);
    double y = this.radius * Math.sin(radiant);

    return new Cartesian(x, y);
  }
}


