package benchmarks.rectanglevariousversions.v0rectangle;

/**
 * From https://docs.oracle.com/javase/7/docs/api/java/awt/geom/AffineTransform.html
 * The AffineTransform class represents a 2D affine transform that performs a linear mapping
 * from 2D coordinates to other  2D coordinates that preserves the "straightness" and
 * "parallelness" of lines.
 * [ x']   [  m00  m01  m02  ] [ x ]   [ m00x + m01y + m02 ]
 * [ y'] = [  m10  m11  m12  ] [ y ] = [ m10x + m11y + m12 ]
 * [ 1 ]   [   0    0    1   ] [ 1 ]   [         1         ]
 */
public class AffineTransform {

  double m00;
  double m10;
  double m01;
  double m11;
  double m02;
  double m12;

  public static AffineTransform getScaleInstance(double sx, double sy) {

    AffineTransform Tx = new AffineTransform();
    Tx.setToScale(sx, sy);
    return Tx;
  }

  public void setToScale(double sx, double sy) {

    m00 = sx;
    m10 = 0.0;
    m01 = 0.0;
    m11 = sy;
    m02 = 0.0;
    m12 = 0.0;
  }

  public double getM00() {
    return m00;
  }

  public double getM10() {
    return m10;
  }

  public double getM01() {
    return m01;
  }

  public double getM11() {
    return m11;
  }

  public double getM02() {
    return m02;
  }

  public double getM12() {
    return m12;
  }
}
