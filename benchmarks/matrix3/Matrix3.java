package benchmarks.matrix3;

/**
 * implementing the transposition and calculation of determinant for matrix of size 3x3
 */
public class Matrix3 {

  //The matrix
  //a b c
  //d e f
  //g h i
  double a, b, c, d, e, f, g, h, i;

  Matrix3(double a, double b, double c, double d, double e, double f, double g, double h, double i) {

    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
    this.e = e;
    this.f = f;
    this.g = g;
    this.h = h;
    this.i = i;
  }

  double determinant() {

    return (a * e * i + b * f * g + c * d * h) - (c * e * g + b * d * i + a * f * h);
  }

  Matrix3 transpose() {

    Matrix3 other = new Matrix3(a, d, g, b, e, h, c, f, i);
    return other;
  }

  double determinantNew() {

    return (a * (e * i) + (g * (b * f) + c * (d * h))) - (e * (c * g) + (i * (b * d) + a * (f * h)));
  }

  double det;

  /**
   * the specification for the transposedSimilar specifies that the determinant of a matrix remains constant if
   * the matrix is transposed.
   */
  /*@
    @ ensures \fp_normal(\result) ==> (\result == det);
    @*/
  double transposedEq() {

    det = determinant();
    return transpose().determinant();
  }

  /*@
    @ ensures \fp_normal(\result) ==> (\result == det);
    @*/
  double transposedEqV2() {

    det = determinantNew();
    return transpose().determinantNew();
  }
}

