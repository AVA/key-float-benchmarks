package benchmarks.rectanglevariousversions.v4simplermathinv2;

public class SimplifiedRectangle2D {

  public double x;
  public double y;



  public SimplifiedRectangle2D(double x, double y) {
    this.x = x;
    this.y = y;
  }

  /**
   * using complicated intervals on test2 but with less computations.
   * gets verified in 6s using cvc4
   */
  /*@ public normal_behaviour
    @  requires arg0.x <= -3.00038 && arg0.x >= -5.00053
    @  && arg0.y <= -3.00038 && arg0.y >= -5.00053
    @  && arg1 <= 4.0000024 && arg1 > 3.0000003001
    @  && arg2 <= 3.0000001 && arg2 > -6.4000000003;
    @  ensures !\fp_nan(\result.x) && !\fp_infinite(\result.x)
    @  && !\fp_nan(\result.y) && !\fp_infinite(\result.y);
    @*/
  public SimplifiedRectangle2D scale(SimplifiedRectangle2D arg0, double arg1, double arg2) {

    double oldX = arg0.getX();
    double oldY = arg0.getY();

    double newX = (oldX * Math.abs(arg1) * Math.abs(arg2));
    double newY = (oldY * Math.abs(arg2) - Math.abs(arg1));

    return new SimplifiedRectangle2D(newX, newY);

  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }
}
