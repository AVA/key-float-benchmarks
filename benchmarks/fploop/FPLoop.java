package benchmarks.fploop;

/**
 * Showcasing how to specify loop behavior in conjunction with floating-point functionality.
 */
public class FPLoop {

  final static double MIN = 1.0;
  final static double SMALL_MAX = 100.0;
  final static double LARGE_MAX = 1.0e30;

  /**
   * The method specification states that the result of the function should be at least as large as the threshold
   */
  /*@
    @ requires xf > MIN;
    @ ensures \result >= SMALL_MAX;
    @*/
  double fploop(double xf) {

    double f = xf;

    /**
     * The loop invariant and variant state that the value will always be larger than 1 and that it will increase
     * in each iteration, respectively.
     */
    /*@
      @ loop_invariant f > MIN;
      @ decreasing (SMALL_MAX - (f - 1.0));
      @*/
    while (f < SMALL_MAX) {
      f = f + 1.0;
    }
    return f;
  }

  /**
   * A modified version of fploop in which The function and contract have been modified to use a larger threshold.
   */
  /*@
    @ requires xf > MIN;
    @ ensures \result >= LARGE_MAX;
    @*/
  double fploop2(double xf) {

    double f = xf;
    /*@
      @ loop_invariant f > MIN;
      @ decreasing (LARGE_MAX - (f - 1.0));
      @*/
    while (f < LARGE_MAX) {
      f = f + 1.0;
    }
    return f;
  }

  /**
   * a hybrid of the previous two examples. The smaller bound is used in the algorithm itself, while the larger bound
   * is used only in the specification in the loop variant.
   */

  /*@
    @ requires xf > MIN;
    @ ensures \result >= SMALL_MAX;
    @*/
  double fploop3(double xf) {

    double f = xf;
    /*@
      @ loop_invariant f > MIN;
      @ decreasing (LARGE_MAX - (f - 1.0));
      @*/
    while (f < SMALL_MAX) {
      f = f + 1.0;
    }
    return f;
  }
}
