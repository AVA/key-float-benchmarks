package benchmarks.cartesianpolar;

public class Cartesian {

  double x;   // Cartesian
  double y;   // coordinates

  // creates and initializes a point with given (x, y)
  public Cartesian(double x, double y) {

    this.x = x;
    this.y = y;
  }

  // return the Euclidean distance between the two points
  /*@ public normal_behaviour
    @ requires \fp_nice(this.x) && \fp_nice(this.y) &&
    @ \fp_nice(that.x) && \fp_nice(that.y);
    @ ensures !\fp_nan(\result);
    @*/
  public double distanceTo(Cartesian that) {

    double dx = this.x - that.x;
    double dy = this.y - that.y;

    return Math.sqrt(dx * dx + dy * dy);
  }

  /*@ public normal_behaviour
    @ requires !\fp_nan(this.x) && !\fp_infinite(this.x) &&
    @ !\fp_nan(this.y) && !\fp_infinite(this.y) && (this.x != 0.0 | this.y != 0.0);
    @ ensures !\fp_nan(\result.radius) && !\fp_nan(\result.theta);
    @*/
  public Polar toPolar() {

    double radius = Math.sqrt((this.x * this.x) + (this.y * this.y));
    double radiant = Math.atan(this.y / this.x);
    double theta = radiant * (180.0 / Math.PI); //angle in degree

    return (new Polar(radius, theta));
  }
}
