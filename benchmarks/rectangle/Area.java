package benchmarks.rectangle;

public class Area {

  Rectangle rectangle2D;

  public Area(Rectangle rectangle2D) {
    this.rectangle2D = rectangle2D;
  }

  //Creates a new Area object that contains the same geometry as this Area transformed by the specified AffineTransform
  //In this case creates a scaled rectangle based on v2
  public Area createTransformedArea(AffineTransform v2) {

    double oldX = this.rectangle2D.getX();
    double oldY = this.rectangle2D.getY();

    double oldUpperRightPointX = oldX + rectangle2D.width;
    double oldUpperRightPointY = oldY;

    double oldLowerLeftPointX = oldX;
    double oldLlowerLeftPointY = oldY - rectangle2D.height;

    double newX = (oldX * v2.getM00()) + (oldY * v2.getM01()) + v2.getM02();
    double newY = (oldX * v2.getM10()) + (oldY * v2.getM11()) + v2.getM12();

    double newUpperRightPointX = (oldUpperRightPointX * v2.getM00()) + (oldUpperRightPointY * v2.getM01()) + v2.getM02();
    double newLowerLeftPointY = (oldLowerLeftPointX * v2.getM10()) + (oldLlowerLeftPointY * v2.getM11()) + v2.getM12();

    return new Area(new Rectangle(newX, newY, Math.abs(newX - newUpperRightPointX), Math.abs(newY - newLowerLeftPointY)));
  }

  public Rectangle getRectangle2D() {
    return rectangle2D;
  }

}
