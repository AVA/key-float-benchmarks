package benchmarks.rotation;

public class Rotation {

  final static double cos90 = 6.123233995736766E-17;
  final static double sin90 = 1.0;

  // rotates a 2D vector by 90 degrees
  public static double[] rotate(double[] vec) {

    double x = vec[0] * cos90 - vec[1] * sin90;
    double y = vec[0] * sin90 + vec[1] * cos90;
    return new double[]{x, y};
  }

  /*@
    @ public normal_behaviour
    @  requires (\forall int i; 0 <= i && i < vec.length; \fp_nice(vec[i]) &&
    @   vec[i] > 1.0 && vec[i] < 2.0) && vec.length == 2;
    @  ensures  \result[0] < 1.0E-15 && \result[1] < 1.0E-15;
    @*/
  public static double[] computeError(double[] vec) {

    double[] temp = rotate(rotate(rotate(rotate(vec))));
    return new double[]{Math.abs(temp[0] - vec[0]), Math.abs(temp[1] - vec[1])};
  }

  /*@
    @ public normal_behaviour
    @  requires (\forall int i; 0 <= i && i < vec.length; \fp_nice(vec[i]) &&
    @   vec[i] > 1.0 && vec[i] < 2.0) && vec.length == 2;
    @  ensures  \result[0] < 1.0E-15 && \result[1] < 1.0E-15;
    @*/
  public static double[] computeRelErr(double[] vec) {

    double[] temp = rotate(rotate(rotate(rotate(vec))));
    return new double[]{Math.abs(temp[0] - vec[0]) / vec[0], Math.abs(temp[1] - vec[1]) / vec[1]};
  }
}