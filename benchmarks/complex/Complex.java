package benchmarks.complex;

public class Complex {

  double realPart;
  double imaginaryPart;

  public Complex(double realPart, double imaginaryPart) {

    this.realPart = realPart;
    this.imaginaryPart = imaginaryPart;
  }

  /*@ public normal_behaviour
    @ requires c.realPart >= -1.0 && c.realPart <= 0.0 &&
    @  this.realPart > -1.0 && this.realPart <= 1.0 && this.imaginaryPart > -2.0 && this.imaginaryPart < 2.0;
    @ ensures !\fp_infinite(this.realPart);
    @
    @ also
    @ requires \fp_nice(c.realPart) && \fp_nice(c.imaginaryPart)
    @ && \fp_nice(this.realPart) && \fp_nice(this.imaginaryPart);
    @ ensures \fp_nice(this.realPart) && \fp_nice(this.imaginaryPart);
    @*/
  public void add(Complex c) {

    this.realPart = this.realPart + c.realPart;
    this.imaginaryPart = this.imaginaryPart + c.imaginaryPart;
  }

  /*@ public normal_behaviour
    @ requires c.realPart== 0.0 && c.imaginaryPart == 0.0;
    @ ensures \fp_nan(this.realPart);
    @
    @ also
    @
    @ requires c.realPart > 1.0  && c.realPart < 50.0 && c.imaginaryPart >  0.0001 && c.imaginaryPart < 10.0 &&
    @  this.realPart > 1.0  && this.realPart < 12.0 && this.imaginaryPart == 0.0;
    @ ensures \fp_nice(\result.realPart) && \fp_nice(\result.imaginaryPart);
    @*/
  public Complex divide(Complex c) {

    double denominator = (c.realPart * c.realPart) + (c.imaginaryPart * c.imaginaryPart);
    this.realPart = ((this.realPart * c.realPart) + (this.imaginaryPart * c.imaginaryPart)) / denominator;
    this.imaginaryPart = ((this.imaginaryPart * c.realPart) - (this.realPart * c.imaginaryPart)) / denominator;

    return this;
  }

  //Lexicographic comparison
  /*@ public normal_behaviour
    @ requires c.realPart >= -1.0 && c.realPart <= 0.0 && c.imaginaryPart > -2.0 && c.imaginaryPart < 0.0 &&
    @  this.realPart > -1.0 && this.realPart <= 1.0 && this.imaginaryPart > -2.0 && this.imaginaryPart < 2.0;
    @ ensures !\fp_infinite(\result);
    @*/
  public double compare(Complex c) {

    if (this.realPart == c.realPart)
      return this.imaginaryPart - c.imaginaryPart;

    else return this.realPart - c.realPart;
  }

  /*@ public normal_behaviour
    @ requires \fp_nice(realPart) && \fp_nice(imaginaryPart) && (realPart != 0.0 && imaginaryPart != 0.0);
    @ ensures !\fp_nan(\result.realPart) && !\fp_nan(\result.imaginaryPart) ;
    @
    @ also
    @
    @ requires realPart == 0.0 && imaginaryPart == 0.0;
    @ ensures \fp_nan(\result.realPart) && \fp_nan(\result.imaginaryPart) ;
    @*/
  public Complex reciprocal() {

    double scale = realPart * realPart + imaginaryPart * imaginaryPart;

    return new Complex(realPart / scale, -imaginaryPart / scale);
  }

  public double getRealPart() {
    return realPart;
  }

  public double getImaginaryPart() {
    return imaginaryPart;
  }
}

