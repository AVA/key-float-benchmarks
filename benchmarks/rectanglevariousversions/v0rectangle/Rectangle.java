package benchmarks.rectanglevariousversions.v0rectangle;

public class Rectangle {

  public double x;

  public double y;

  public double width;

  public double height;

  /**
   * A Rectangle specifies an area in a coordinate space that is enclosed by the Rectangle
   * object's upper-left point (x,y) in the coordinate space, its width, and its height.
   */
  public Rectangle(double x, double y, double width, double height) {

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  /**
   * Returns a scaled version of rectangle arg0 based on the affine transformation made by
   * arg1 and arg2.
   * Contract 0 verifies and Contract 1 results in a counterexample
   */
  /*@ public normal_behaviour
    @  requires arg0.x <= -3.38 && arg0.x >= -5.53
    @  && arg0.y <= -3.38 && arg0.y >= -5.53
    @  && arg0.width <= 3.7332 && arg0.width > 3.1
    @  && arg0.height <= 4.0004 && arg0.height > 3.0000001
    @  && arg1 <= 4.0024 && arg1 > 3.0003001
    @  && arg2 <= 3.0001 && arg2 > -6.4000003;
    @  ensures !\fp_nan(\result.x) && !\fp_infinite(\result.x)
    @  && !\fp_nan(\result.y) && !\fp_infinite(\result.y)
    @  && !\fp_nan(\result.width) && !\fp_infinite(\result.width)
    @  && !\fp_nan(\result.height) && !\fp_infinite(\result.height);
    @*/
  public static Rectangle scale(Rectangle arg0, double arg1, double arg2) {

    Area v1 = new Area(arg0);
    AffineTransform v2 = AffineTransform.getScaleInstance(arg1, arg2);
    Area v3 = v1.createTransformedArea(v2);
    Rectangle v4 = v3.getRectangle2D();
    return v4;
  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }
}
