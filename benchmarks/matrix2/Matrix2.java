package benchmarks.matrix2;

/**
 * implementing the transposition and calculation of determinant for matrix of size 2x2
 */
public class Matrix2 {

  //The matrix
  // a b
  // c d
  double a, b, c, d;

  Matrix2(double a, double b, double c, double d) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.d = d;
  }

  double determinant() {
    return (a * d - b * c);
  }

  Matrix2 transpose() {
    return new Matrix2(a, c, b, d);
  }

  double det;

  /**
   * calculates the determinant before and after a matrix is transposed, and its contract states that the equality
   * holds.
   */
  /*@
    @ ensures \fp_normal(\result) ==> (\result == det);
    @*/
  double transposedEq() {
    det = determinant();
    return transpose().determinant();
  }
}
