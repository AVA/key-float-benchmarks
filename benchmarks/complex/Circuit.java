package benchmarks.complex;

//Analysis of RL circuit
public class Circuit {

  double maxVoltage;
  double frequency;
  double resistance;
  double inductance;

  /*@ public normal_behaviour
    @ requires  this.frequency > 1.0 && this.frequency < 100.0 && this.resistance > 1.0  && this.resistance < 50.0 &&
    @  this.inductance > 0.001 && this.inductance < 0.004;
    @ ensures \fp_nice(\result.imaginaryPart) && \result.imaginaryPart < 10.0 && \result.imaginaryPart > 0.0001;
    @*/
  public Complex computeImpedance() {

    return new Complex(resistance, 2.0 * Math.PI * frequency * inductance);
  }

  /*@ public normal_behaviour
    @ requires this.maxVoltage > 1.0  && this.maxVoltage < 12.0 && this.frequency > 1.0 && this.frequency < 100.0 &&
    @  this.resistance > 1.0  && this.resistance < 50.0 && this.inductance > 0.001 &&
    @  this.inductance < 0.004;
    @ ensures \fp_nice(\result.realPart) && \fp_nice(\result.imaginaryPart);
    @
    @ also
    @
    @ requires this.maxVoltage > 1.0  && this.maxVoltage < 12.0 && this.frequency > 1.0 && this.frequency < 100.0 &&
    @  this.resistance > 1.0  && this.resistance < 50.0 && this.inductance > 0.001 &&
    @  this.inductance < 0.004;
    @ ensures (\result.realPart != 0.0) || (\result.imaginaryPart !=0.0);
    @*/
  public Complex ComputeCurrent() {

    return new Complex(maxVoltage, 0.0).divide(computeImpedance());
  }

  //computes instantaneous current
  /*@ public normal_behaviour
    @ requires this.maxVoltage > 1.0  && this.maxVoltage < 12.0 && this.frequency > 1.0 && this.frequency < 100.0 &&
    @  this.resistance > 1.0  && this.resistance < 50.0 && this.inductance > 0.001 &&
    @  this.inductance < 0.004 && time > 0.0 && time < 300.0;
    @ ensures \fp_nice(\result);
    @*/
  public double computeInstantCurrent(double time) {

    Complex current = ComputeCurrent();
    double maxCurrent = Math.sqrt(current.getRealPart() * current.getRealPart() + current.getImaginaryPart() * current.getImaginaryPart());
    double theta = Math.atan(current.getImaginaryPart() / current.getRealPart());

    return maxCurrent * Math.cos((2.0 * Math.PI * frequency * time) + theta);
  }

  //computes instantaneous voltage
  /*@ public normal_behaviour
    @ requires this.maxVoltage > 1.0  && this.maxVoltage < 12.0 && this.frequency > 1.0 &&
    @  this.frequency < 100.0 && time > 0.0 && time < 300.0;
    @ ensures \fp_nice(\result);
    @*/
  public double computeInstantVoltage(double time) {

    return maxVoltage * Math.cos(2.0 * Math.PI * frequency * time);
  }
}
