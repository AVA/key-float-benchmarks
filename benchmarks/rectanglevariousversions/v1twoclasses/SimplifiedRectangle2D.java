package benchmarks.rectanglevariousversions.v1twoclasses;


public class SimplifiedRectangle2D {

  public double x;
  public double y;
  public double width;
  public double height;


  public SimplifiedRectangle2D(double x, double y, double width, double height) {

    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
  }

  /**
   * Simplified version of the scalerectangle example by reducing the classes into two.
   * gets verified in 27s using cvc4
   */
  /*@ public normal_behaviour
    @  requires arg0.x <= -3.38 && arg0.x >= -5.53
    @  && arg0.y <= -3.38 && arg0.y >= -5.53
    @  && arg0.width <= 3.7332 && arg0.width > 3.1
    @  && arg0.height <= 4.0004 && arg0.height > 3.0000001
    @  && arg1 <= 4.0024 && arg1 > 3.0003001
    @  && arg2 <= 3.0001 && arg2 > -6.4000003;
    @  ensures !\fp_nan(\result.x) && !\fp_infinite(\result.x)
    @  && !\fp_nan(\result.y) && !\fp_infinite(\result.y)
    @  && !\fp_nan(\result.width) && !\fp_infinite(\result.width)
    @  && !\fp_nan(\result.height) && !\fp_infinite(\result.height);
    @*/
  public SimplifiedRectangle2D scale(SimplifiedRectangle2D arg0, double arg1, double arg2) {

    AffineTransform v2 = AffineTransform.getScaleInstance(arg1, arg2);
    double oldX = arg0.getX();
    double oldY = arg0.getY();

    double oldUpperRightPointX = oldX + arg0.width;
    double oldUpperRightPointY = oldY;

    double oldLowerLeftPointX = oldX;
    double oldLlowerLeftPointY = oldY - arg0.height;

    double newX = (oldX * v2.getM00()) + (oldY * v2.getM01()) + v2.getM02();
    double newY = (oldX * v2.getM10()) + (oldY * v2.getM11()) + v2.getM12();

    double newUpperRightPointX = (oldUpperRightPointX * v2.getM00()) + (oldUpperRightPointY * v2.getM01()) + v2.getM02();
    double newLowerLeftPointY = (oldLowerLeftPointX * v2.getM10()) + (oldLlowerLeftPointY * v2.getM11()) + v2.getM12();


    return new SimplifiedRectangle2D(newX, newY, Math.abs(newX - newUpperRightPointX), Math.abs(newY - newLowerLeftPointY));

  }

  public double getX() {
    return x;
  }

  public double getY() {
    return y;
  }
}
